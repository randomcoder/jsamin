/**
 * Created by tim on 21/06/15.
 */
window.animFrame = (function (callback) {
  return window.requestAnimationFrame ||
    window.mozRequestAnimationFrame ||
    window.webkitRequestAnimationFrame ||
    window.oRequestAnimationFrame ||
    window.msRequestAnimationFrame ||
    function (callback) {
      window.setTimeout(callback, 1000 / 60);
    };
})();

function nextColour(currentColour) {
  if (currentColour == "red") {
    return "green";
  }
  else if (currentColour == "green") {
    return "blue";
  }
  else if (currentColour == "blue") {
    return "purple";
  }
  else {
    return "red"
  }
}

function drawText(text, context) {
  context.font = text.style + " " + text.size + " " + text.font;
  context.fillStyle = text.fillColour;
  context.fillText(text.text, text.x, text.y);
}

function animate(text, context) {
  drawText(text, context);
  text.fillColour = nextColour(text.fillColour);

  var timeout = 250;

  setTimeout(function(){
    animFrame(function () {
      animate(text, context);
    });
  }, timeout);
}

function drawTriangle(triangle, context) {
  var shift = triangle.sideLength / 2;
  console.log("Drawing triangle at (" + triangle.centreX + ", " + triangle.centreY + ")");
  context.beginPath();
  lineTo(shift, shift, context);
  lineTo(0, -1 * shift, context);
  lineTo(-1 * shift, shift, context);
  lineTo(shift, shift, context);
  context.closePath();

  context.lineWidth = 2;
  context.strokeStyle = triangle.fillColour;
  context.stroke();
}

function lineTo(x, y, context) {
  console.log("Drawing line to (" + x + ", " + y + ")")
  context.lineTo(x, y)
  console.log("Moving to (" + x + ", " + y + ")")
  context.moveTo(x, y)
}

function updateRotation(original) {
  var newRotation = original + Math.PI / 180;

  if (newRotation >= Math.PI * 2) {
    newRotation = Math.PI * -2;
  }

  return newRotation;
}

function updateTriangle(triangle) {
  triangle.rotation = updateRotation(triangle.rotation);
  triangle.fillColour = nextColour(triangle.fillColour);
}

function animateTriangle(triangle, context, canvas) {
  context.clearRect(0, 0, canvas.width, canvas.height);

  context.save();

  context.translate(canvas.width / 2, canvas.height / 2);
  context.rotate(triangle.rotation);
  drawTriangle(triangle, context);

  context.restore();

  updateTriangle(triangle);

  animFrame(function () {
    animateTriangle(triangle, context, canvas);
  });
}

var theText = {
  text: "Hello",
  size: '40pt',
  style: 'bold',
  font: 'Arial',
  fillColour: 'red',
  x: 100,
  y: 100
};

var theTriangle = {
  sideLength: 40,
  centreX: 0,
  centreY: 0,
  rotation: 0,
  fillColour: 'red'
};

var theCanvas = document.getElementById("textcanvas");
var context = theCanvas.getContext('2d');

drawText(theText, context);

setTimeout(function () {
  animate(theText, context);
}, 1000);

var triangleCanvas = document.getElementById("trianglecanvas");
var triangleContext = triangleCanvas.getContext('2d');

setTimeout(function() {
  animateTriangle(theTriangle, triangleContext, triangleCanvas);
}, 100);
